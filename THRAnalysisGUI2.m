function varargout = THRAnalysisGUI2(varargin)
% THRANALYSISGUI2 MATLAB code for THRAnalysisGUI2.fig
%      THRANALYSISGUI2, by itself, creates a new THRANALYSISGUI2 or raises the existing
%      singleton*.
%
%      H = THRANALYSISGUI2 returns the handle to a new THRANALYSISGUI2 or the handle to
%      the existing singleton*.
%
%      THRANALYSISGUI2('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in THRANALYSISGUI2.M with the given input arguments.
%
%      THRANALYSISGUI2('Property','Value',...) creates a new THRANALYSISGUI2 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before THRAnalysisGUI2_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to THRAnalysisGUI2_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help THRAnalysisGUI2

% Last Modified by GUIDE v2.5 16-Nov-2018 16:22:47

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @THRAnalysisGUI2_OpeningFcn, ...
    'gui_OutputFcn',  @THRAnalysisGUI2_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before THRAnalysisGUI2 is made visible.
function THRAnalysisGUI2_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to THRAnalysisGUI2 (see VARARGIN)

% Choose default command line output for THRAnalysisGUI2
handles.output = hObject;
% Update handles structure
guidata(hObject, handles);
% UIWAIT makes THRAnalysisGUI2 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = THRAnalysisGUI2_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if ~isfield(handles,'selpath')
    handles.selpath = uigetdir;
    guidata(hObject, handles);
else
    handles.selpath = uigetdir(handles.selpath);
end
analyze_data(hObject,handles);


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fid = fopen([handles.selpath '\' handles.sess_file_name '_Peformance.txt'],'w');
fprintf(fid,handles.full_performance_str);
fclose(fid);

% --- Executes during object creation, after setting all properties.
function axes1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: place code in OpeningFcn to populate axes1
handles.axes1 = findobj(gcf,'Tag','axes1');


% --- Executes during object creation, after setting all properties.
function axes2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: place code in OpeningFcn to populate axes2
handles.axes2 = findobj(gcf,'Tag','axes2');


% --- Executes during object creation, after setting all properties.
function axes3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: place code in OpeningFcn to populate axes3
handles.axes3 = findobj(gcf,'Tag','axes3');


% --- Executes during object creation, after setting all properties.
function text2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called



function analyze_data(hObject,handles)
folderpath = handles.selpath;
dashes = strfind(folderpath,'\');

log_dir = folderpath(1:dashes(length(dashes)));
sess_file_name = folderpath(dashes(length(dashes))+1:end);

trialinfoname = '\TrialInfoLogs_';
trialinfodata = tdfread([folderpath trialinfoname sess_file_name '.txt']);

%% Plot Reaction time distribution
rts = trialinfodata.Time2BlueTouch_RT;
holddurs = trialinfodata.HoldDuration;

rts(rts == -1.000) = [];
rts(rts > 1) = [];
rts = rts*1000;
holddurs(holddurs == -1.000) = [];
holddurs(holddurs > 2) = [];
holddurs = 1000*holddurs;

rt5pc = round(prctile(rts,5));
rt10pc = round(prctile(rts,10));
rt25pc = round(prctile(rts,25));
rt50pc = round(prctile(rts,50));
rt75pc = round(prctile(rts,75));
rt95pc = round(prctile(rts,95));

axes(handles.axes1); % Switch current axes to axes1.
hist(rts,35)
box off 
ylabel('Trial Count')
xlabel('Reaction Time (ms)')
title(['Reaction Time (ms) Distribution: 50%: ' num2str(rt50pc) ' ms'])

%% PLot hold duraiton distribution
hd5pc = round(prctile(holddurs,5));
hd10pc = round(prctile(holddurs,10));
hd25pc = round(prctile(holddurs,25));
hd50pc = round(prctile(holddurs,50));
hd75pc = round(prctile(holddurs,75));
hd95pc = round(prctile(holddurs,95));

axes(handles.axes2); % Switch current axes to axes2.
hist(holddurs,35)
box off 
ylabel('Count')
xlabel('Hold duration (ms)')
title(['Hold Duration (ms) Distribution: 50%: ' num2str(hd50pc) ' ms'])
%% Plot smoothed performance

proportionCorrect = trialinfodata.proportionCorrect;
correct_incorrect = NaN(1,length(proportionCorrect));
for t = 1:length(proportionCorrect)
   if t == 1
       if proportionCorrect(t) == 1
           correct_incorrect(t) = 1;
       else
           correct_incorrect(t) = 0;
       end
   else
       if proportionCorrect(t) > proportionCorrect(t-1)
           correct_incorrect(t) = 1;
       else
           correct_incorrect(t) = 0;
       end
   end
end
smoothed_correct_incorrect = filtfilt(1/10*ones(1,10),1,...
    [correct_incorrect(10:-1:1) correct_incorrect correct_incorrect(end:-1:end-9)]);
smoothed_correct_incorrect = smoothed_correct_incorrect(11:end-10);

axes(handles.axes3); % Switch current axes to axes3.
plot(100*smoothed_correct_incorrect)
box off 
xlim([0 length(smoothed_correct_incorrect)+1])
xlabel('Trial #')
ylabel('Performance (%)')
title('Smoothed Performance')

%% Create Performance str
%%Text box text
trial_count = length(correct_incorrect);
peformance_str = ['\t\t Completed ' num2str(trial_count) ' trials \n' ...
    '\t Average Performance was ' num2str(100*sum(correct_incorrect)/trial_count,3) '%% \n' ...
    '---------------------------------------------------------\n'];
  
hd_str = ['Hold Duration 5%% = ' num2str(hd5pc,3) ' (ms)\n' ...
    'Hold Duration 10%% = ' num2str(hd10pc,3) ' (ms)\n' ...
    'Hold Duration 25%% = ' num2str(hd25pc,3) ' (ms)\n' ...
    'Hold Duration 50%% = ' num2str(hd50pc,3) ' (ms)\n' ...
    'Hold Duration 75%% = ' num2str(hd75pc,3) ' (ms)\n' ...
    'Hold Duration 95%% = ' num2str(hd95pc,3) ' (ms)\n', ...
    '---------------------------------------------------------\n'];
  
rt_str = ['Reaction Time 5%% = ' num2str(rt5pc,3) ' (ms)\n' ...
    'Reaction Time 10%% = ' num2str(rt10pc,3) ' (ms)\n' ...
    'Reaction Time 25%% = ' num2str(rt25pc,3) ' (ms)\n' ...
    'Reaction Time 50%% = ' num2str(rt50pc,3) ' (ms)\n' ...
    'Reaction Time 75%% = ' num2str(rt75pc,3) ' (ms)\n' ...
    'Reaction Time 95%% = ' num2str(rt95pc,3) ' (ms)\n'];

full_performance_str = ['\n\t\t' sess_file_name ' \n\n ' peformance_str hd_str rt_str];
set(handles.text2,'string',sprintf(full_performance_str));

handles.sess_file_name = sess_file_name;
handles.full_performance_str = full_performance_str;
guidata(hObject, handles);
return

%%

% --- Executes when figure1 is resized.
function figure1_SizeChangedFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
