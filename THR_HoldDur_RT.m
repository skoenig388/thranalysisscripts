% %THR EventLog_Reader
% %Draft written 6/2/2018 SDK
clear,clc,close all
dir = 'Z:\DATA_kiosk\Reider\Touch-Hold-Release Task\THR-Reider-083-83-2019-08-2--10-37-07\';
dash = strfind(dir,'\');
dash = dash(end-1);
basename = ['_' dir(dash+1:end-1)];


trialinfoname = 'TrialINfoLogs';
configname = 'ConfigLogs';

txtname = '.txt';

trialinfodata = tdfread([dir trialinfoname basename txtname]);
configdata = tdfread([dir configname basename txtname]);

%%
rts = trialinfodata.Time2BlueTouch_RT;
holddurs = trialinfodata.HoldDuration;

rts(rts == -1.000) = [];
rts(rts > 1) = [];
rts = rts*1000;
holddurs(holddurs == -1.000) = [];
holddurs(holddurs > 2) = [];
holddurs = 1000*holddurs;
%%
rt5pc = round(prctile(rts,5));
rt10pc = round(prctile(rts,10));
rt25pc = round(prctile(rts,25));
rt50pc = round(prctile(rts,50));
rt75pc = round(prctile(rts,75));
rt95pc = round(prctile(rts,95));

figure
subplot(1,2,1)
hist(rts,35)
box off 
ylabel('Count')
xlabel('Reaction Time (ms)')
title(['Reaction Time Distribution::   5%: ' num2str(rt5pc) ',    25%: ' num2str(rt25pc) ...
    ',     50%: ' num2str(rt50pc) ',    75%: ' num2str(rt75pc)])

%%
hd5pc = round(prctile(holddurs,5));
hd10pc = round(prctile(holddurs,10));
hd25pc = round(prctile(holddurs,25));
hd50pc = round(prctile(holddurs,50));
hd75pc = round(prctile(holddurs,75));
hd95pc = round(prctile(holddurs,95));

subplot(1,2,2)
hist(holddurs,35)
box off 
ylabel('Count')
xlabel('Hold duration (ms)')
title(['Hold Duration Distribution::   5%: ' num2str(hd5pc) ',    25%: ' num2str(hd25pc) ...
    ',    50%: ' num2str(hd50pc) ',    75%: ' num2str(hd75pc)])
%%
%%
rcts = rts;
rcts = [rcts(10:-1:1); rcts; rcts(end:-1:end-9)];
rcts = filtfilt(1/10*ones(1,10),1,rcts);
rcts = rcts(10:end-10);

[r,p] = corrcoef(1:length(rts),rts);

figure
subplot(2,2,1)
plot(rcts)
xlabel('Trial #')
ylabel('Hold duration (ms)')
title(sprintf(['Smoothed Reaction time by trial # \nr = ' num2str(r(2),3) ', p = ' num2str(p(2),3) ]))
box off
%%
hds = holddurs;
hds = [hds(10:-1:1); hds; hds(end:-1:end-9)];
hds = filtfilt(1/10*ones(1,10),1,hds);
hds = hds(10:end-10);

[r,p] = corrcoef(1:length(holddurs),holddurs);

subplot(2,2,2)
plot(hds)
xlabel('Trial #')
ylabel('Hold duration (ms)')
title(sprintf(['Smoothed Hold duration by trial # \nr = ' num2str(r(2),3) ', p = ' num2str(p(2),3) ]))
box off
%%
proportionCorrect = trialinfodata.proportionCorrect;
correct_incorrect = NaN(1,length(proportionCorrect));
for t = 1:length(proportionCorrect)
   if t == 1
       if proportionCorrect(t) == 1
           correct_incorrect(t) = 1;
       else
           correct_incorrect(t) = 0;
       end
   else
       if proportionCorrect(t) > proportionCorrect(t-1)
           correct_incorrect(t) = 1;
       else
           correct_incorrect(t) = 0;
       end
   end
end

%%
smoothed_correct_incorrect = filtfilt(1/10*ones(1,10),1,...
    [correct_incorrect(10:-1:1) correct_incorrect correct_incorrect(end:-1:end-9)]);
smoothed_correct_incorrect = smoothed_correct_incorrect(11:end-10);

[r,p] = corrcoef(1:length(smoothed_correct_incorrect),smoothed_correct_incorrect);

subplot(2,2,3)
plot(smoothed_correct_incorrect)
xlabel('Trial #')
ylabel('Smoothed Performance')
title(sprintf(['THR smoothed performance \nr = ' num2str(r(2),3) ', p = ' num2str(p(2),3)]))
box off

%%
figure
plot(trialinfodata.proportionCorrect)
xlabel('Trial #')
ylabel('Cumulative % Performance')
box off
ylim([0.4 1])
%% Plot Relevant Config Variales
configtrial = configdata.trialCount;
whitedur = configdata.BlinkOffDuration;
bluedur = configdata.BlinkOnDuration;
mintouchduration = configdata.MinTouchDurationSec;
maxtrouchduration = configdata.MaxTouchDurationSec;
Release2Reward = configdata.ReleaseToRewardDelayMS;
squaresize = configdata.BlueSquareSize;
itidur = configdata.ItiDuration;
rewardpulses = configdata.RewardCount;

figure
subplot(3,3,1)
plot(configtrial,whitedur)
xlabel('Trial #')
ylabel('White Duration')
box off
xlim([0 max(configtrial)+1])

subplot(3,3,2)
plot(configtrial,bluedur)
xlabel('Trial #')
ylabel('Blue Duration')
box off
xlim([0 max(configtrial)+1])

subplot(3,3,3)
plot(configtrial,mintouchduration)
xlabel('Trial #')
ylabel('Min Touch Duration')
box off
xlim([0 max(configtrial)+1])

subplot(3,3,4)
plot(configtrial,maxtrouchduration)
xlabel('Trial #')
ylabel('Max Touch Duration')
box off
xlim([0 max(configtrial)+1])

subplot(3,3,5)
plot(configtrial,Release2Reward)
xlabel('Trial #')
ylabel('Release to Rwd Duration')
box off
xlim([0 max(configtrial)+1])

subplot(3,3,6)
plot(configtrial,squaresize)
xlabel('Trial #')
ylabel('Square Size')
box off
xlim([0 max(configtrial)+1])

subplot(3,3,7)
plot(configtrial,itidur)
xlabel('Trial #')
ylabel('ITI duration')
box off
xlim([0 max(configtrial)+1])

subplot(3,3,8)
plot(configtrial,rewardpulses)
xlabel('Trial #')
ylabel('Rwd Pulse Count')
box off
xlim([0 max(configtrial)+1])
