%Written by Seth Koenig to pull in unspecified THR data
%4/19/19

smooth_window = 3;

%---Pearl---%
% data_dir = 'Z:\DATA_kiosk\Pearl\Touch-Hold-Release Task\';
% monkey_name = 'Pearl';

%---Wotan---%
%cuts off really about 1/2 through training
% data_dir = 'Z:\MonkeyData\Wotan\Touch-Hold-Release Task\';
% monkey_name = 'Wotan';

%---Frey---%
%cuts off near end of 400 ms hold training
% data_dir = 'Z:\MonkeyData\Frey\Touch-Hold-Release Task\';
% monkey_name = 'Frey';

%---Igor---%
data_dir = 'Z:\DATA_kiosk\Reider\Touch-Hold-Release Task\';
monkey_name = 'Reider';


%---Reider---%
% data_dir = 'Z:\MonkeyData\Frey\Touch-Hold-Release Task\';
% monkey_name = 'Frey';

file_list = dir(data_dir);

average_hold_duration_by_session = NaN(1,length(file_list));
average_peformance_by_session = NaN(1,length(file_list));
minimum_hold_duration_by_session = NaN(1,length(file_list));
trial_count_by_session = NaN(1,length(file_list));
session_number = NaN(1,length(file_list));
session_date = NaN(1,length(file_list));
for file = 1:length(file_list)
    if contains(file_list(file).name,'THR')
        disp(['Processing File #' num2str(file) '/' num2str(length(file_list))]);
       
        try
            trialinfodata = tdfread([data_dir file_list(file).name '\TrialInfoLogs_' file_list(file).name '.txt']);
            configdata = tdfread([data_dir file_list(file).name '\ConfigLogs_' file_list(file).name '.txt']);
        catch
            disp(['Cant open or find ' [data_dir file_list(file).name '\TrialInfoLogs_' file_list(file).name '.txt']])
            continue
        end
        
        if size( trialinfodata.HoldDuration,1) < 25
            continue
        end
        
        dashes = strfind(file_list(file).name,'-');
        num = file_list(file).name(dashes(2)+1:dashes(3)-1);
        session_number(file) = str2double(num);
        
        year = str2double(file_list(file).name(dashes(4)+1:dashes(5)-1));
        day = str2double(file_list(file).name(dashes(5)+1:dashes(6)-1));
        month = str2double(file_list(file).name(dashes(6)+1:dashes(7)-1));
        minute = str2double(file_list(file).name(dashes(8)+1:dashes(9)-1));
        session_date(file) = year*10000+month*100+day+minute/100;
        
        proportionCorrect = trialinfodata.proportionCorrect;
        correct_incorrect = NaN(1,length(proportionCorrect));
        for t = 1:length(proportionCorrect)
            if t == 1
                if proportionCorrect(t) == 1
                    correct_incorrect(t) = 1;
                else
                    correct_incorrect(t) = 0;
                end
            else
                if proportionCorrect(t) > proportionCorrect(t-1)
                    correct_incorrect(t) = 1;
                else
                    correct_incorrect(t) = 0;
                end
            end
        end
        
        trial_count = length(correct_incorrect);
        average_peformance_by_session(file) = 100*sum(correct_incorrect)/trial_count;
        trial_count_by_session(file) = trial_count;
        
        mintouchduration = configdata.MinTouchDurationSec;
        minimum_hold_duration_by_session(file) = nanmedian(mintouchduration);
        
        holddurs = trialinfodata.HoldDuration;
        holddurs(holddurs == -1.000) = [];
        holddurs(holddurs > 2) = [];
        holddurs = 1000*holddurs;
        average_hold_duration_by_session(file) = nanmedian(holddurs);
        
    end
end
%% Clean up NaNs
average_hold_duration_by_session = laundry(average_hold_duration_by_session);
average_peformance_by_session = laundry(average_peformance_by_session);

minimum_hold_duration_by_session = laundry(minimum_hold_duration_by_session);
trial_count_by_session = laundry(trial_count_by_session);
session_number = laundry(session_number);
session_date = laundry(session_date);
%% Reorder so all files are in order
%[~,session_order] = sort(session_number);
[~,session_order] = sort(session_date);
%%
average_hold_duration_by_session = average_hold_duration_by_session(session_order);
average_peformance_by_session = average_peformance_by_session(session_order);
minimum_hold_duration_by_session = minimum_hold_duration_by_session(session_order);
trial_count_by_session = trial_count_by_session(session_order);
%% Smooth a few variables of interest
smoothed_trial = trial_count_by_session;
smoothed_trial = [smoothed_trial(10:-1:1) smoothed_trial smoothed_trial(end:-1:end-9)];
smoothed_trial = filtfilt(1/smooth_window*ones(1,smooth_window),1,smoothed_trial);
smoothed_trial = smoothed_trial(11:end-10);

smoothed_hold_durs = average_hold_duration_by_session;
smoothed_hold_durs = [smoothed_hold_durs(10:-1:1) smoothed_hold_durs smoothed_hold_durs(end:-1:end-9)];
smoothed_hold_durs = filtfilt(1/smooth_window*ones(1,smooth_window),1,smoothed_hold_durs);
smoothed_hold_durs = smoothed_hold_durs(11:end-10);

smoothed_perform = average_peformance_by_session;
smoothed_perform = [smoothed_perform(10:-1:1) smoothed_perform smoothed_perform(end:-1:end-9)];
smoothed_perform = filtfilt(1/smooth_window*ones(1,smooth_window),1,smoothed_perform);
smoothed_perform = smoothed_perform(11:end-10);
%%
num_sessions = length(average_hold_duration_by_session);

figure


subplot(2,2,1)
plot(smoothed_trial)
xlim([0 num_sessions+1]);
xlabel('Session #')
ylabel('# of Trials Completed')
box off

subplot(2,2,2)
plot(smoothed_hold_durs)
xlim([0 num_sessions+1]);
xlabel('Session #')
ylabel('Average Hold Duration (ms)')
box off

subplot(2,2,3)
plot(1000*minimum_hold_duration_by_session)
xlim([0 num_sessions+1]);
xlabel('Session #')
ylabel('Miniumum Hold Duration (ms)')
box off
ylim([0 500])

subplot(2,2,4)
plot(smoothed_perform)
xlim([0 num_sessions+1]);
xlabel('Session #')
ylabel('Average Peformance (% Correct)')
box off


subtitle(monkey_name);