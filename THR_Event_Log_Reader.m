% %THR EventLog_Reader
% %Draft written 6/2/2018 SDK
%
clear,clc

dir = 'Z:\MonkeyData\Frey\THR-Frey-1-1-2018-17-8--11-12-04\';
dash = strfind(dir,'\');
dash = dash(end-1);
basename = ['_' dir(dash+1:end-1)];
%basename = '';
eventname = 'EventLogs';
configname = 'ConfigLogs';
performancename = 'PerformanceMonitorLog';
trialinfoname = 'TrialInfoLogs';
txtname = '.txt';

eventdata = tdfread([dir eventname basename txtname]);
configdata = tdfread([dir configname basename txtname]);
performancedata = tdfread([dir performancename basename txtname]);
trialinfodata = tdfread([dir trialinfoname basename txtname]);

%%
tme = eventdata.FrameStartTime;
Framecount = eventdata.Frame;
trialcount = eventdata.trialCount;
rewardcountrelease = eventdata.numRewardedReleases;
rewardcounttouch = eventdata.numRewardedTouches;
touchdown = eventdata.isTouchDownOrHold;
touchreleased = eventdata.isTouchReleased;
%%
try
    rewardpulsecount = eventdata.RewardPulseCount;
catch
    rewardpulsecount = [];
end
%%
touchx = eventdata.TouchX;
touchx(touchx == -99999) = NaN;
touchx = touchx-1920;%touchscreen is second screen so first screen x:[0 1920]
touchy = eventdata.TouchY;
touchy(touchy == -99999) = NaN;

figure
 plot(touchx)
hold on
plot(touchy)
legend('X','Y')
xlabel('Sample #')
ylabel('Position (pixels?)')

%%
figure
plot(diff(tme))
xlabel('Sample #')
ylabel('Time between samples (seconds)')
ylim([0 0.032])
%%
tchange = 31500;%approximate
maxsamples = length(tme);
tchange2 = tme(tchange);
tend = tme(end);
%% #samples/amount of time
storagerateb4lag = tchange/tchange2;
storagerateAfterlag = (maxsamples-tchange)/(tend-tchange2);

%%
touchdown10 = NaN(1,length(touchdown));
touchreleased10 = NaN(1,length(touchreleased));
for s = 1:length(touchdown)
   if ~isempty(strfind(touchdown(s,:),'True'))
       touchdown10(s) = 1;
   else
       touchdown10(s) = 0;
   end

      if ~isempty(strfind(touchreleased(s,:),'True'))
       touchreleased10(s) = 1;
   else
       touchreleased10(s) = 0;
   end
end
%%
trialind = NaN(2,max(trialcount));
for t = 1:max(trialcount)
   ind = find(trialcount == t);
   trialind(1,t) = ind(1);
   trialind(2,t) = ind(end);
end

%%
reward_count_change_by_trial = NaN(1,max(trialcount));
for t = 1:max(trialcount)
    rewards = eventdata.RewardPulseCount(trialind(1,t):trialind(2,t));
    reward_count_change_by_trial(t) = rewards(end)-rewards(1);
end

figure
plot(reward_count_change_by_trial)
xlabel('Trial #')
ylabel('Reward Count')